using Pirates;
using System.Collections.Generic;
using System.Linq;

//TODO go to the edge of the conquering areainstead of center (unless there are enemies)
//TODO (this might already be a feature) maybe damaged ships should run away from enemy ships with more health
//TODO if enemy tanking drones is a problem, send pirates to destroy clusters of drones

/*****************************************************************************************************************************************************
#DEBUG - A hashtag used to declare a debug. Those lines should be deleted/changed to a comment, since the players against us can see the debugs too. *
                                                                                                                                                     *
                                                                                                                                                     *
Allowed Libraries:                                                                                                                                   *
                                                                                                                                                     *
System.Collections.Generic                                                                                                                           *
System.Linq                                                                                                                                          *
System.Text                                                                                                                                          *
System.Collections                                                                                                                                   *
System.Math                                                                                                                                          *
System.Random                                                                                                                                        *
System.Array                                                                                                                                         *
System.IComparable                                                                                                                                   *
System.Enum                                                                                                                                          *
Pirates                                                                                                                                              *
MyBot                                                                                                                                                *
                                                                                                                                                     *
*****************************************************************************************************************************************************/

namespace MyBot
{
    public class MyBot : IPirateBot
    {
        #region CONSTANTS
        const bool PRINT_DEBUGS = true;
        const int MAX_DISTANCE_BETWEEN_2_FRIENDLY_DRONES = 6;
        const int RANGE_BETWEEN_DRONE_TO_CLOSEST_CITY = 8;
        const int NUMBER_OF_DRONES_IN_TANK = 4;
        const int ENEMY_SIEGER_DISTANCE_FROM_BASE = 3;
        const int DRONE_FLEE_FROM_ENEMY_DIST = 4;
        const int DRONE_FIGHT_WAY_TO_CITY_RANGE = DRONE_FLEE_FROM_ENEMY_DIST + 1;
        const int ENEMY_IS_CLOSE_TO_DRONE_MAX_DISTANCE = 6;
        const int DRONE_IS_CLOSE_TO_CITY_MAX_DISTANCE = 6;
        const int PIRATE_IS_CLOSE_TO_ISLAND_MAX_DISTANCE = 6;
        const int RANGE_MODIFIER_FOR_NEUTRAL_CITY_PRIORITY = 5;
        const int RANGE_BETWEEN_CLOSE_PIRATES = 5;
        const int CLOSE_PAINTBALL = 10;
        private const int DISTANCE_BETWEEN_TWO_PIRATES_IN_WHICH_THEY_ARE_CONSIDERED_ABOUT_TO_FIGHT_SRY_FR_LNG_NAME = 6;
        bool _madeDecoy = false;
        const int NUMBER_OF_DRONES_CLOSE_TOGETHER = 3;
        #endregion

        #region GLOBALS
        PirateGame _game;
        Pirate _enemySieger;
        Pirate _decoyedPirate;
        Location _decoyedPirateDestination;
        List<Aircraft> _attackedAircrafts;
        #endregion

        /// <summary>
        /// Enumeration of all map objects in the game
        /// </summary>
        public enum mapobjects
        {
            FriendlyDrone,
            FriendlyPirate,
            FriendlyIsland,
            FriendlyCity,
            EnemyDrone,
            EnemyPirate,
            EnemyIsland,
            EnemyCity,
            NeutralCity,
            Paintball
            //maybe add neutral islands, 
        };

        /// <summary>
        /// Returns a list of all gameObjects in the game of the type specified 
        /// </summary>
        public List<MapObject> getListOf(mapobjects o)
        {
            List<MapObject> container = new List<MapObject>();

            switch (o)
            {
                case mapobjects.FriendlyPirate:
                    container = _game.GetMyLivingPirates().Cast<MapObject>().ToList();
                    break;
                case mapobjects.FriendlyDrone:
                    container = _game.GetMyLivingDrones().Cast<MapObject>().ToList();
                    break;
                case mapobjects.FriendlyIsland:
                    container = _game.GetMyIslands().Cast<MapObject>().ToList();
                    break;
                case mapobjects.FriendlyCity:
                    container = _game.GetMyCities().Cast<MapObject>().ToList();
                    break;
                case mapobjects.EnemyDrone:
                    container = _game.GetEnemyLivingDrones().Cast<MapObject>().ToList();
                    break;
                case mapobjects.EnemyPirate:
                    container = _game.GetEnemyLivingPirates().Cast<MapObject>().ToList();
                    break;
                case mapobjects.EnemyIsland:
                    container = _game.GetEnemyIslands().Cast<MapObject>().ToList();
                    break;
                case mapobjects.EnemyCity:
                    container = _game.GetEnemyCities().Cast<MapObject>().ToList();
                    break;
                case mapobjects.NeutralCity:
                    container = _game.GetNeutralCities().Cast<MapObject>().ToList();
                    break;
                case mapobjects.Paintball:
                    container = _game.GetAllPaintballs().Cast<MapObject>().ToList();
                    break;
            }
            return container;
        }

        /// <summary>
        /// Returns a Pair consisting of the closest mapobject (specified) to the object 'from', and the distance between them
        /// </summary>
        public DistObjPair getClosest(mapobjects o, MapObject from)
        {
            int minDist = -1;
            MapObject closestObj = null;
            List<MapObject> container = getListOf(o);

            foreach (MapObject obj in container)
            {
                int currDist = obj.Distance(from);
                if (minDist == -1 || currDist < minDist)
                {
                    minDist = currDist;
                    closestObj = obj;
                }
            }

            return new DistObjPair(minDist, closestObj);
        }

        /// <summary>
        /// Print message to screen during the game view, if PRINT_DEBUGS is set to true
        /// </summary>
        public void Debug(string debugMessage)
        {
            if (PRINT_DEBUGS) _game.Debug(debugMessage);
        }

        /// <summary>
        /// Returns a drone which is currently in the center of a cluster. The cluster contains at least 'minDrones' drones.
        /// </summary>
        public Drone GetCenterOfDroneCluster(int minDrones)
        {
            foreach (Drone center in _game.GetEnemyLivingDrones())
            {
                int count = 0;
                foreach (Drone drone in _game.GetEnemyLivingDrones())
                {
                    if (drone.Distance(center) <= _game.GetPaintballRange())
                    {
                        count++;
                    }
                }
                if (count >= minDrones)
                {
                    return center;
                }
            }
            return null;
        }

        #region ALONS_FUNCTIONS
        void AlonsHandleDrones()
        {
            List<Drone> drones = _game.GetMyLivingDrones();
            int i = 0;

            foreach (Drone currDrone in _game.GetMyLivingDrones())
            {
                Location destination = FindCityForUnloading(currDrone).Location;

                // Get the sail options to the city
                List<Location> sailOptions = _game.GetSailOptions(currDrone, destination);
                // Set sail!
                if (i == 0)
                {
                    _game.SetSail(currDrone, sailOptions[0]);
                }
                else
                {
                    _game.SetSail(currDrone, sailOptions[sailOptions.Count - 1]);
                }
                i++;
            }
        }

        bool AlonsTryPaintball(Pirate pirate)
        {
            int closestDist = 1000;
            City cityToSiege = _game.GetEnemyCities()[0];
            if (pirate.HasPaintball)
            {
                Drone closestDrone = null;
                foreach (Drone drone in _game.GetEnemyLivingDrones())
                {
                    if (pirate.Distance(drone) < closestDist)
                    {
                        closestDist = pirate.Distance(drone);
                        closestDrone = drone;
                    }
                }

                //todo try to be in the center before exploding
                if (closestDrone != null && cityToSiege.Distance(closestDrone) < 20)
                {
                    //if (pirate.InAttackRange(closestDrone))
                    //{
                    //    _game.Attack(pirate, closestDrone);
                    //    Debug("pirate " + pirate.ToString() + " paintballing " + closestDrone.ToString());
                    //}
                    //else
                    //{
                    //    List<Location> sailOptions = _game.GetSailOptions(pirate, closestDrone);
                    //    _game.SetSail(pirate, sailOptions[0]);
                    //}
                    //return true;
                    SearchAndDestroy(pirate, closestDrone);
                    return true;
                }
                else
                {
                    Location guardingLocation = new Location(22, 36);
                    Sail(guardingLocation, pirate);
                    Debug("yolo alon tyry paintball going to guarding location");
                    return true;
                }
            }
            else
            {
                Pirate closestPirate = null;
                foreach (Paintball paintball in _game.GetAvailablePaintballs())
                {
                    foreach (Pirate currPirate in _game.GetMyLivingPirates())
                    {
                        if (!currPirate.HasPaintball)
                        {
                            if (currPirate.Distance(paintball) < closestDist)
                            {
                                closestDist = currPirate.Distance(paintball);
                                closestPirate = currPirate;
                            }
                        }
                    }

                    if (closestPirate == pirate)
                    {
                        if (paintball.IsAvailable(_game.GetTurn() + closestDist / pirate.MaxSpeed))
                        {
                            List<Location> sailOptions = _game.GetSailOptions(pirate, paintball);
                            _game.SetSail(pirate, sailOptions[0]);
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        public bool AlonsSiege(Pirate pirate)
        {
            City cityToSiege = _game.GetEnemyCities()[0];
            Pirate sieger = (Pirate) getClosest(mapobjects.FriendlyPirate, cityToSiege).obj;

            if (pirate == sieger)
            {
                if (pirate.HasPaintball)
                {
                    if (AlonsTryPaintball(pirate))
                        return true;
                }
                else if (!DoesAnyPirateHaveAPaintball())
                {
                    if (AlonsTryPaintball(pirate))
                        return true;
                }
                else
                {
                    Paintball closeBall = (Paintball)getClosest(mapobjects.Paintball, pirate).obj;
                    if (closeBall != null && !DoesAnyPirateHaveAPaintball())
                    {
                        Sail(closeBall.Location, pirate);
                        return true;
                    }
                }
            }
            return false;
        }

        public void AlonsHandlePirates()
        {
            //CityPiratePair siegerPiratePair = AssignSieger();
            //int siegerId = siegerPiratePair.pirateId;
            //City siegerTarget = siegerPiratePair.city;
            Location location = new Location(10, 23);
            Pirate loner = (Pirate)getClosest(mapobjects.FriendlyPirate, location).obj;//GetFarthestMyPirate(GetIslandById(3));

            foreach (Pirate pirate in _game.GetMyLivingPirates())
            {
                if (IsCloseToEnemyPirate(pirate))
                {
                    if (TryDecoy(pirate))
                        continue;
                }

                if (TryAttack(pirate))
                    continue;

                if (pirate == loner)
                {
                    TryLoner(pirate);
                    continue;
                }

                if (_game.GetTurn() <= 6)
                {
                    Location anchorPoint = _game.GetMyself().Id == 1 ? new Location(38, 40) : new Location(36, 8);
                    Sail(new Location(38, 40), pirate);
                    continue;
                }
                else
                {
                    Sail(new Location(34, 23), pirate);
                    continue;
                }
            }
        }
        #endregion

        /// <summary>
        /// Returns the farthest Friendly pirate from specified island
        /// </summary>
        private Pirate GetFarthestMyPirate(Island island)
        {
            int farthestDistance = -1;
            Pirate farthestPirate = null;
            foreach (Pirate pirate in _game.GetMyLivingPirates())
            {
                if (pirate.Distance(island) > farthestDistance)
                {
                    farthestDistance = pirate.Distance(island);
                    farthestPirate = pirate;
                }
            }
            return farthestPirate;
        }

        private Island GetIslandById(int id)
        {
            foreach (Island island in _game.GetAllIslands())
            {
                if (id == island.Id)
                    return island;
            }
            return null;
        }

        private bool TryLoner(Pirate pirate)
        {
            Island toConquer = GetClosestNotMyIslandExcluding3(pirate);

            if (toConquer == null)
            {
                Debug("error, no notmyislands");
                return false;
            }
            else
            {
                Sail(toConquer.Location, pirate);
                return true;
            }
        }

        private Island GetClosestNotMyIslandExcluding3(MapObject mo)
        {
            Island closestIsland = null;
            int closestDist = 999;

            foreach (Island island in _game.GetNotMyIslands())
            {
                if (island.Id == 3) //excluding island 3
                    continue;

                if (island.Distance(mo) < closestDist)
                {
                    closestDist = island.Distance(mo);
                    closestIsland = island;
                }
            }

            return closestIsland;
        }

        public bool Paintballing(Pirate pirate)
        {
            if (pirate.HasPaintball)
            {
                if (TryPaintball(pirate))
                    return true;
            }
            else if (!DoesAnyPirateHaveAPaintball())
            {
                if (TryPaintball(pirate))
                    return true;
            }
            else
            {
                Paintball closeBall = (Paintball)getClosest(mapobjects.Paintball, pirate).obj;
                if (closeBall != null && !DoesAnyPirateHaveAPaintball())
                {
                    Sail(closeBall.Location, pirate);
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Entry point for each turn
        /// </summary>
        public void DoTurn(PirateGame Game)
        {
            _attackedAircrafts = new List<Aircraft>();
            this._game = Game;
            Debug(_game.GetEnemy().BotName);
            DecoyFieldsInitialization();

            if (_game.GetEnemy().BotName == "12217") //aka hafifa
            {
                HafifaInitialization();
                BeatHafifa();
            }
            else if (_game.GetEnemy().BotName == "12218") // aka mits'ad
            {
                BeatMitsad();
            }
            else
            {
                //HandlePirates();
                AlonsHandlePirates();
                AlonsHandleDrones();
                //HandleDrones();
                HandleDecoy();
            }
        }

        private void DecoyFieldsInitialization()
        {
            if (_decoyedPirate != null)
            {
                if (!_decoyedPirate.IsAlive())
                {
                    _decoyedPirate = null;
                }
            }
            _madeDecoy = false;
            _decoyedPirateDestination = null;
        }

        /// <summary>
        /// Tells pirates what to do based on priorities
        /// </summary>
        void HandlePirates()
        {
            CityPiratePair siegerPiratePair = AssignSieger();
            int siegerId = siegerPiratePair.pirateId;
            City siegerTarget = siegerPiratePair.city;

            foreach (Pirate pirate in _game.GetMyLivingPirates())
            {
                if (IsCloseToEnemyPirate(pirate))
                {
                    if (TryDecoy(pirate))
                        continue;
                }

                if (pirate.HasPaintball)
                {
                    if (TryPaintball(pirate))
                        continue;
                }
                else if (!DoesAnyPirateHaveAPaintball())
                {
                    if (TryPaintball(pirate))
                        continue;
                }

                if (TryAttack(pirate))
                    continue;

                if (_game.GetEnemy().BotName != "12217") //aka hafifa
                {
                    if (pirate.Id == siegerId && siegerTarget != null)
                    {
                        SiegeEnemy(pirate, siegerTarget);
                        Debug("pirate " + siegerId.ToString() + " sieging...");
                        continue;
                    }
                }

                //if (TryTeamAttack(pirate))
                //    continue;

                //if (Siege(pirate))
                //    continue;

                /* if (Bodyguard(pirate))
                    continue;*/

                if (TryConquer(pirate))
                    continue;
            }
        }

        /// <summary>
        /// Command given pirate to sail to given destination
        /// </summary>
        private void Sail(Location location, Pirate pirate)
        {
            if (pirate == _leader)//this is for Hafifa bot
            {
                _leaderDestination = location;
            }
            if (location == null)
            {
                Debug("Sail: location parameter is null, not doing anything");
            }
            else
            {
                DiagonalSail(pirate, _game.GetSailOptions(pirate, location));
            }
        }

        /// <summary>
        /// Decides who will siege what island
        /// </summary>
        CityPiratePair AssignSieger()
        {
            // a sieger is needed once the enemy has a drone
            if (_game.GetEnemy().BotName == "12112") //dafuq is dis shit TODO
            {
                return new CityPiratePair(-1, null);
            }
            if (_game.GetEnemyLivingDrones().Count == 0)
                return new CityPiratePair(-1, null);

            City cityToSiege = null;
            int closeDroneDist = 999;

            //find the city that has the minimum distance to an enemy drone
            foreach (City tempCity in _game.GetNotMyCities())
            {
                DistObjPair currDrone = getClosest(mapobjects.EnemyDrone, tempCity);
                if (currDrone.distance < closeDroneDist)
                {
                    cityToSiege = tempCity;
                    closeDroneDist = currDrone.distance;
                }
            }
            
            //tuple Item 1: distance to city. Item 2: closest pirate/drone to city
            DistObjPair closeDrone = getClosest(mapobjects.EnemyDrone, cityToSiege);
            DistObjPair closePirate = getClosest(mapobjects.FriendlyPirate, cityToSiege);

            Debug("closest Drone dist: " + closeDrone.distance.ToString() + ", id: " + ((Drone)closeDrone.obj).Id.ToString());
            Debug("closest Pirate dist: " + closePirate.distance.ToString() + ", id: " + ((Pirate)closePirate.obj).Id.ToString());

            // siege if enemy drone is close enough to unload
            if (closeDrone.distance <= MAX_DISTANCE_BETWEEN_2_FRIENDLY_DRONES)
                return new CityPiratePair(((Pirate)closePirate.obj).Id, cityToSiege);

            //siege if enemy drone is closer to enemy city than closest pirate
            if (closeDrone.distance - 3 < closePirate.distance)
                return new CityPiratePair(((Pirate)closePirate.obj).Id, cityToSiege);

            return new CityPiratePair(-1, null);
        }

        private bool IsCloseToEnemyPirate(Pirate pirate)
        {
            bool isCloseToEnemyPirate = false;
            foreach (Pirate enemyPirate in _game.GetEnemyLivingPirates())
            {
                if (enemyPirate.InRange(pirate, pirate.AttackRange + 4))
                    isCloseToEnemyPirate = true;
            }

            return isCloseToEnemyPirate;
        }

        private bool TryConquer(Pirate pirate)
        {
            List<IslandPiratePair> islandPiratePairList = GetIslandPiratePairList();
            // Choose destination
            foreach (IslandPiratePair pair in islandPiratePairList)
            {
                if (pair.pirateId == pirate.Id)
                {
                    Sail(pair.island.Location, pirate);
                    return true;
                }
            }

            return false;
        }

        public bool Siege(Pirate pirate)
        {
            List<City> citiesToSiege = _game.GetEnemyCities();
            citiesToSiege.AddRange(_game.GetNeutralCities());

            foreach (City cityToSiege in citiesToSiege)
            {
                if (IsThreatened(cityToSiege))
                {
                    Drone closestDrone = (Drone)getClosest(mapobjects.EnemyDrone, cityToSiege).obj;
                    Pirate closestPirateToDrone = (Pirate)getClosest(mapobjects.FriendlyPirate, closestDrone).obj;

                    if (closestPirateToDrone == pirate)
                    {
                        Debug("sending pirate: " + pirate.Id.ToString() + " to take down drone: " + closestDrone.Id.ToString());
                        SearchAndDestroy(closestPirateToDrone, closestDrone);
                        return true;
                    }
                }
            }

            return false;
        }

        public bool IsThreatened(City cityToSiege)
        {
            DistObjPair closestDrone = getClosest(mapobjects.EnemyDrone, cityToSiege);

            if (closestDrone.obj == null)
                return false;

            else if (closestDrone.distance < 20)
            {
                Debug("city: " + cityToSiege.Id.ToString() + " is threatened by drone: " + closestDrone.obj.ToString());
                return true;
            }

            else
                return false;
        }

        private bool TryTeamAttack(Pirate myPirate)
        {
            /*
            foreach (Pirate enemyPirateCloseToMyPirate in GetEnemyPiratesInRangeOfLocation(myPirate.Location, RANGE_BETWEEN_CLOSE_PIRATES))
            {
                foreach (Pirate currMyPirate in _game.GetMyLivingPirates())
                {
                    if (currMyPirate.InRange(enemyPirateCloseToMyPirate, RANGE_BETWEEN_CLOSE_PIRATES) && currMyPirate != myPirate)
                    {
                        Debug("pirate id: " + myPirate.Id.ToString() + " is teamattacking pirate " + enemyPirateCloseToMyPirate.Id.ToString());
                        SearchAndDestroy(myPirate, enemyPirateCloseToMyPirate);
                        return true;
                    }
                }
            }*/
            List<Pirate> myPirates = _game.GetMyLivingPirates();
            myPirates.Remove(myPirate);

            foreach (Pirate enemyPirate in _game.GetEnemyLivingPirates())
            {
                foreach (Pirate friendlyPirate in _game.GetMyLivingPirates())
                {
                    if (friendlyPirate.InRange(enemyPirate, RANGE_BETWEEN_CLOSE_PIRATES) && enemyPirate.Distance(myPirate) <= RANGE_BETWEEN_CLOSE_PIRATES)
                    {
                        SearchAndDestroy(myPirate, enemyPirate);
                        return true;
                    }
                }
            }

            return false;
        }

        public void SearchAndDestroy(Pirate hunter, Aircraft target)
        {
            if (hunter.InAttackRange(target))
            {
                //TODO check if the health is 0
                _attackedAircrafts.Add(target);
                _game.Attack(hunter, target);
            }
            else
            {
                List<Location> sailOptions = _game.GetSailOptions(hunter, target);
                DiagonalSail(hunter, sailOptions);
            }
        }

        public List<Pirate> GetEnemyPiratesInRangeOfLocation(Location location, int maxRange)
        {
            List<Pirate> EnemiesInRange = new List<Pirate>();

            foreach (Pirate enemyPirate in _game.GetEnemyLivingPirates())
            {
                if (location.InRange(enemyPirate, maxRange))
                    EnemiesInRange.Add(enemyPirate);
            }

            return EnemiesInRange;
        }
        /// <summary>
        /// sends drones to the best city for them to unload
        /// <remark>
        /// sail method: -advances on the path that is least crowded by enemy pirates possible
        ///              -if close to enemy pirate, changes direction and runs away from him
        /// </remark></summary>
        void HandleDrones()
        {
            foreach (Drone d in _game.GetMyLivingDrones())
            {
                //check if needs to flee from enemy
                City unload = FindCityForUnloading(d);
                DistObjPair closeEnemy = getClosest(mapobjects.EnemyPirate, d);

                if (closeEnemy.distance < DRONE_FLEE_FROM_ENEMY_DIST && d.Distance(unload) > DRONE_FIGHT_WAY_TO_CITY_RANGE)
                    flee(d, (Pirate)closeEnemy.obj);

                else
                {
                    List<Location> options = _game.GetSailOptions(d, unload);

                    //choose the loneliest of the two options
                    int chosenLocationIndex = getLoneliestOption(options);

                    //sails as far as possible from enemy pirates
                    _game.SetSail(d, options[chosenLocationIndex]);
                }
            }
        }

        /// <summary>
        /// Returns the index of the location which is farthest away from enemy pirates
        /// </summary>
        int getLoneliestOption(List<Location> options)
        {
            //choose the loneliest of the two options
            int chosenLocationIndex = 0;
            if (options.Count > 1)
            {
                int distanceOp0 = getClosest(mapobjects.EnemyPirate, options[0]).distance;
                int distanceOp1 = getClosest(mapobjects.EnemyPirate, options[1]).distance;
                if (distanceOp1 > distanceOp0)
                    chosenLocationIndex = 1;
            }
            return chosenLocationIndex;
        }

        /// <summary>
        /// Make specified drone flee away from the specified enemy
        /// </summary>
        void flee(Drone drone, Pirate Enemy)
        {
            //how it works: gives the drone a new destination which is calculated by: 2 * drone_coordinates - enemy_coordinates
            //this calculation will always yield a location which is in the opposite direction of the enemy
            int newRow = 2 * drone.Location.Row - Enemy.Location.Row;
            int newCol = 2 * drone.Location.Col - Enemy.Location.Col;

            //handling out-of-board cases
            int maxRow = _game.GetRowCount();
            int maxCol = _game.GetColCount();
            newRow = newRow < 0 ? 0 : newRow;
            newCol = newCol < 0 ? 0 : newCol;
            newRow = newRow > maxRow ? maxRow : newRow;
            newCol = newCol > maxCol ? maxCol : newCol;

            //sailing to destination
            List<Location> options = _game.GetSailOptions(drone, new Location(newRow, newCol));

            //choose the loneliest of the two options
            int chosenLocationIndex = getLoneliestOption(options);

            //sails as far as possible from enemy pirates
            _game.SetSail(drone, options[chosenLocationIndex]);
        }

        bool IsCloseEnemy(Drone drone)
        {
            foreach (Pirate enemyP in _game.GetEnemyLivingPirates())
            {
                if (drone.InRange(enemyP, ENEMY_IS_CLOSE_TO_DRONE_MAX_DISTANCE))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Returns the best city for specified Drone to sail to
        /// </summary>
        City FindCityForUnloading(Drone drone)
        {
            int closestMyDist = 999;
            int closestNeutralDist = 999;
            City closestMyCity = (City)getClosest(mapobjects.FriendlyCity, drone).obj;
            City closestNeutralCity = (City)getClosest(mapobjects.NeutralCity, drone).obj;
            int rangeOfPriority = RANGE_MODIFIER_FOR_NEUTRAL_CITY_PRIORITY;

            //if we have 19, while the maximum is 20, it doesn't matter if the drone gives us 1 or 2 points, we win either way.
            if (closestNeutralCity != null)
            {
                if (_game.GetMyScore() > _game.GetMaxPoints() - closestNeutralCity.ValueMultiplier)
                {
                    Debug("drone: " + drone.Id.ToString() + " ignores the fact that neutral city gives more points because its irrelevent");
                    rangeOfPriority = 0;
                }
            }

            if (closestNeutralDist < closestMyDist + rangeOfPriority)
                return closestNeutralCity;
            else
                return closestMyCity;
        }

        /*This function return the number of living friendly Drones*/
        int NumOfDrones(Location location)
        {
            int count = 0;
            List<Drone> drones = _game.GetMyLivingDrones();
            for (int i = 0; i < drones.Count; i++)
            {
                if (drones[i].GetLocation().Col == location.Col && drones[i].GetLocation().Row == location.Row)
                {
                    count += 1;
                }
            }
            return count;
        }

        Drone GetMyClosestDrone(Drone drone)
        {
            foreach (Drone myDrone in _game.GetMyLivingDrones())
            {
                if (myDrone.Id != drone.Id)
                {
                    if (myDrone.GetLocation().Col != drone.GetLocation().Col || myDrone.GetLocation().Row != drone.GetLocation().Row)
                    {
                        if (drone.InRange(myDrone, MAX_DISTANCE_BETWEEN_2_FRIENDLY_DRONES))
                        {
                            return myDrone;
                        }
                    }
                }
            }
            return null;
        }

        //in the original implementation it got "pirate" for some reason
        Drone GetOtherDrone(Drone originalDrone)
        {
            foreach (Drone currDrone in _game.GetMyLivingDrones())
            {
                if (originalDrone.Id != currDrone.Id &&
                    originalDrone.GetLocation() != currDrone.GetLocation() &&
                    originalDrone.InRange(currDrone, MAX_DISTANCE_BETWEEN_2_FRIENDLY_DRONES))
                {
                    return currDrone;
                }
            }
            return null;
        }

        //TODO: who calls this function????
        Aircraft GetAircraftToAttack(Pirate pirate, TypeOfAircraft aircraftType)
        {
            Aircraft attackCraft = null;
            int biggestHealth = _game.GetPirateMaxHealth() + 1;
            List<Aircraft> targets;

            //TODO try to use the this 2 identical blocks of code just once
            if (aircraftType == TypeOfAircraft.Pirates)
            {
                targets = _game.GetEnemyLivingPirates().Cast<Aircraft>().ToList();
            }
            else
            {
                targets = _game.GetEnemyLivingAircrafts();
            }

            foreach (Aircraft enemyAircraft in targets)
            {
                //check if the enemy is in attack range
                if (pirate.InAttackRange(enemyAircraft))
                {
                    if (enemyAircraft.CurrentHealth < biggestHealth)
                    {
                        attackCraft = enemyAircraft;
                        biggestHealth = enemyAircraft.CurrentHealth;
                    }
                }
            }

            return attackCraft;
        }

        int GetEffectiveLifeTotal(Aircraft aircraft)
        {
            int currHealth = aircraft.CurrentHealth;
            foreach (Aircraft attackedAircraft in _attackedAircrafts)
            {
                if (attackedAircraft == aircraft) //every time the aircraft appears there, it means it is attacked this turn.
                {
                    currHealth--;
                }
                //for example, if a pirate with 2 health, appears here twice, it will return 0. 
            }

            return currHealth;
        }
        
        //TODO add prioritization on the enemy pirate with least amount of hp, sync multiple attacks on the same ship, prevent things like: 2 ships attack the same ship with 1 hp (or a drone)
        //makes pirate attack only if an enemy is on its attack range
        bool TryAttack(Pirate pirate)
        {
            Aircraft aircraftToAttack = null;
            int lowestHealth = 6;

            //TODO use the same foreach twice
            foreach (Pirate enemyPirate in _game.GetEnemyLivingPirates())
            {
                if (pirate.InAttackRange(enemyPirate))
                {
                    int effectiveLifeTotal = GetEffectiveLifeTotal(enemyPirate);
                    if (effectiveLifeTotal < lowestHealth && effectiveLifeTotal > 0) //should attacked the most damaged pirate, unless it will be dead anyway.
                    {
                        aircraftToAttack = enemyPirate;
                        lowestHealth = effectiveLifeTotal;
                    }
                }
            }

            //only search for a drone if no pirate was found (cus were prioritizing pirates)
            if (aircraftToAttack == null)
            {
                foreach (Drone enemyDrone in _game.GetEnemyLivingDrones())
                {
                    int effectiveLifeTotal = GetEffectiveLifeTotal(enemyDrone);
                    if (pirate.InAttackRange(enemyDrone))
                    {
                        if (effectiveLifeTotal < lowestHealth && effectiveLifeTotal > 0)
                        {
                            aircraftToAttack = enemyDrone;
                            lowestHealth = effectiveLifeTotal;
                        }
                    }
                }
            }

            if (aircraftToAttack != null)
            {
                _attackedAircrafts.Add(aircraftToAttack);
                _game.Attack(pirate, aircraftToAttack);
                Debug("pirate " + pirate.ToString() + " attacks " + aircraftToAttack.ToString());
                return true;
            }
            return false;
        }

        void SiegeEnemy(Pirate sieger, City cityToSiege)
        {
            //commands a (friendly) pirate to go and siege the enemy's city
            if (!TryAttack(sieger))
            {
                List<Location> sailOptions = _game.GetSailOptions(sieger, cityToSiege);

                foreach (Drone drone in _game.GetEnemyLivingDrones())
                {
                    //TODO predict where the drone will be and go there instead of the current drone location
                    //TODO use a const instead of 8
                    if (drone.InRange(cityToSiege, cityToSiege.UnloadRange + RANGE_BETWEEN_DRONE_TO_CLOSEST_CITY))
                    {
                        Location destination = drone.GetLocation();
                        sailOptions = _game.GetSailOptions(sieger, destination);
                    }
                }

                DiagonalSail(sieger, sailOptions);
            }
        }

        bool IsEnemySieging()
        {
            if (_game.GetMyCities().Count >= 1)
            {
                foreach (Pirate enemyPirate in _game.GetEnemyLivingPirates())
                {
                    foreach (City myCurrCity in _game.GetMyCities())
                    {
                        if (enemyPirate.GetLocation().InRange(myCurrCity, myCurrCity.UnloadRange + ENEMY_SIEGER_DISTANCE_FROM_BASE))
                        {
                            //assign this so our destroyer will know who to destroy
                            _enemySieger = enemyPirate;
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        //assign the pirate that is closest to the drone closest to a city to defend that drone.
        bool Bodyguard(Pirate pirate)
        {
            int closestDist = 999;
            foreach (City city in _game.GetMyCities()) //TODO count neutral city aswell
            {
                Drone closestDrone = GetMyClosestDrone(city);
                if (closestDrone != null)
                {
                    //checking all pirates to see whos the closest to the drone
                    foreach (Pirate currPirate in _game.GetMyLivingPirates())
                    {
                        if (currPirate.Distance(closestDrone) < closestDist)
                        {
                            closestDist = currPirate.Distance(closestDrone);
                        }
                    }

                    //only the closest pirate to the drone will be a bodyguard
                    if (closestDist == pirate.Distance(closestDrone))
                    {
                        Escort(pirate, closestDrone, city);
                        return true;
                    }
                }


            }
            return false;
        }

        private void Escort(Pirate pirate, Drone closestDrone, City city)
        {
            Debug("priate: " + pirate.Id.ToString() + " escorting");
            Location destination;
            if (pirate.Location == closestDrone.Location)
            {
                destination = city.Location;
            }
            else
            {
                destination = closestDrone.Location;
            }

            Sail(destination, pirate);
        }

        private Drone GetMyClosestDrone(City city)
        {
            int closestDist = 999;
            Drone closest = null;
            foreach (Drone drone in _game.GetMyLivingDrones())
            {
                if (drone.Distance(city) < closestDist)
                {
                    closestDist = drone.Distance(city);
                    closest = drone;
                }
            }
            return closest;
        }

        private bool IsSieged(City city)
        {
            foreach (Pirate enemy in _game.GetEnemyLivingPirates())
            {
                if (enemy.Distance(city) < 5)
                    return true;
            }
            return false;
        }

        int NumOfShows<T>(List<T> list, T value)
        {
            int count = 0;
            foreach (T item in list)
            {
                if (item.Equals(value))
                {
                    count++;
                }
            }
            return count;
        }

        void DiagonalSail(Pirate pirate, List<Location> sailOptions)
        {
            Location destination = sailOptions.Count > 1 ? sailOptions[1] : sailOptions[0];

            _game.SetSail(pirate, destination);
            if (_decoyedPirate != null)
                if (pirate.Id == _decoyedPirate.Id)
                    _decoyedPirateDestination = destination;
        }

        List<IslandPiratePair> GetIslandPiratePairList()
        {
            Debug("getislandpiratepairlist:");
            List<IslandPiratePair> retList = new List<IslandPiratePair>();
            List<Pirate> pirateList = _game.GetAllMyPirates(); //TODO try living pirates

            while (pirateList.Count > 0) //continues until every pirate gets assigned an island
            {
                if (_game.GetNotMyIslands().Count == 0)
                    break;

                foreach (Island island in _game.GetNotMyIslands())
                {
                    int closestDist = 999;
                    Pirate closestPirate = null;
                    foreach (Pirate pirate in pirateList)
                    {
                        if (island.Distance(pirate) < closestDist)
                        {
                            closestDist = island.Distance(pirate);
                            closestPirate = pirate;
                        }
                    }

                    if (closestPirate != null)
                    {
                        Debug("closest pirate to island: " + island.Id.ToString() + " is pirate: " + closestPirate.Id.ToString());
                        pirateList.Remove(closestPirate);
                        IslandPiratePair temp = new IslandPiratePair(closestPirate.Id, island);
                        retList.Add(temp);
                    }
                }
            }

            return retList;
        }

        bool DoesAnyPirateHaveAPaintball()
        {
            foreach (Pirate pirate in _game.GetMyLivingPirates())
            {
                if (pirate.HasPaintball)
                    return true;
            }
            return false;
        }

        bool AreSomeDronesClose()
        {
            int count;
            foreach (Drone drone in _game.GetEnemyLivingDrones())
            {
                count = 0;
                foreach (Drone currDrone in _game.GetEnemyLivingDrones())
                {
                    if (drone.Distance(currDrone) <= _game.GetPaintballRange())
                        count++;
                    if (count == NUMBER_OF_DRONES_CLOSE_TOGETHER)
                        return true;
                }
            }
            return false;
        }

        bool TryPaintball(Pirate pirate)
        {
            int closestDist = 1000;

            if (pirate.HasPaintball)
            {
                Drone closestDrone = null;
                foreach (Drone drone in _game.GetEnemyLivingDrones())
                {
                    if (pirate.Distance(drone) < closestDist)
                    {
                        closestDist = pirate.Distance(drone);
                        closestDrone = drone;
                    }
                }

                if (closestDrone != null)
                {
                    if (pirate.InAttackRange(closestDrone))
                    {
                        _game.Attack(pirate, closestDrone);
                        Debug("pirate " + pirate.ToString() + " paintballing " + closestDrone.ToString());
                    }
                    else
                    {
                        List<Location> sailOptions = _game.GetSailOptions(pirate, closestDrone);
                        _game.SetSail(pirate, sailOptions[0]);
                    }
                    return true;
                }
            }
            else
            {
                Pirate closestPirate = null;
                foreach (Paintball paintball in _game.GetAvailablePaintballs())
                {
                    if (AreSomeDronesClose())
                    {
                        foreach (Pirate currPirate in _game.GetMyLivingPirates())
                        {
                            if (!currPirate.HasPaintball)
                            {
                                if (currPirate.Distance(paintball) < closestDist)
                                {
                                    closestDist = currPirate.Distance(paintball);
                                    closestPirate = currPirate;
                                }
                            }
                        }

                        if (closestPirate == pirate)
                        {
                            if (paintball.IsAvailable(_game.GetTurn() + closestDist / pirate.MaxSpeed))
                            {
                                List<Location> sailOptions = _game.GetSailOptions(pirate, paintball);
                                _game.SetSail(pirate, sailOptions[0]);
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        }

        //TODO LIST:
        //3. prefer decoying a higher health ship EDIT: this probably won't make a difference
        //6. if the decoy was never attacked after a while (enemy probably knows its a decoy), go for a suicide mission (maybe a friendly ship can kill the decoy) (so we can create a second decoy).
        //7. (maybe) baiting: decoy goes near enemy ship and causes it to chase it (And the decoy runs away)
        bool TryDecoy(Pirate pirate)
        {
            // Check if the player can decoy a pirate
            if (pirate.Owner.TurnsToDecoyReload == 0 && !_madeDecoy)
            {
                // Whoosh
                _game.Decoy(pirate);
                // print a message
                Debug(pirate + " decoys");
                // Did decoy
                _madeDecoy = true;
                _decoyedPirate = pirate;
                //Debug("@@@@@@@@@@@@@@@@updated _decoyedPirate to pirate: " + _decoyedPirate.Id.ToString());
                return true;
            }

            // didn't decoy
            return false;
        }

        //TODO currently follows the real one 1 turn behind, this is terrible. but can be easily (i think) fixed after we make a pirate wrapper, we can have a field containing the destination of the ship, and the decoy will go there instead of the current location of the ship.
        void HandleDecoy()
        {
            Decoy decoy = _game.GetMyDecoy();
            if (decoy != null)
            {
                //Debug("@@@@@@@@@@handledecoy:");
                Debug((_decoyedPirateDestination == null).ToString());
                Debug((_decoyedPirate == null).ToString());
                if (_decoyedPirateDestination != null)
                {
                    DiagonalSail(decoy, _game.GetSailOptions(decoy, _decoyedPirateDestination));
                    //_game.SetSail(decoy, _decoyedPirateDestination);
                }
                else if (!_decoyedPirate.IsAlive())
                {
                    int closestDist = 999;
                    Pirate closestFriendlyShip = null;

                    foreach (Pirate friendlyShip in _game.GetAllMyPirates())
                    {
                        if (decoy.Distance(friendlyShip) < closestDist)
                        {
                            closestDist = decoy.Distance(friendlyShip);
                            closestFriendlyShip = friendlyShip;
                        }
                    }

                    List<Location> sailOptions = _game.GetSailOptions(decoy, closestFriendlyShip);

                    DiagonalSail(decoy, sailOptions);
                    Debug("decoy " + decoy.ToString() + " sails to " + closestFriendlyShip.ToString());
                }
            }
        }
     
        //fields for beating hafifa and mitsad
        Pirate _leader;
        Location _leaderDestination;

        private void HafifaInitialization()
        {
            _leader = null;
            _leaderDestination = null;
        }

        private void BeatHafifa()
        {
            int leftiestColumn = 999;
            foreach (Pirate myPirate in _game.GetMyLivingPirates())
            {
                if (myPirate.Distance(new Location(14, 27)) < leftiestColumn)
                {
                    _leader = myPirate;
                    leftiestColumn = myPirate.Distance(new Location(14, 27));
                }
            }
            Debug("leader: " + _leader.Id);


            bool done = false;

            if (IsCloseToEnemyPirate(_leader))
            {
                if (TryDecoy(_leader))
                    done = true;
            }

            if (!done)
            {
                if (TryAttack(_leader))
                    done = true;
            }

            if (!done)
            {
                if (GetNumberOfFriendlyPiratesInLeaderSpot() >= 3)
                {
                    Sail(new Location(14, 27), _leader); //the closest spot in which you can conquer both islands
                }
                else
                {
                    Sail(new Location(14, 35), _leader); //the edge of the closer island
                }
            }

            foreach (Pirate pirate in _game.GetMyLivingPirates())
            {
                if (pirate != _leader)//that pirate already did something
                {
                    if (IsCloseToEnemyPirate(pirate))
                    {
                        if (TryDecoy(pirate))
                            continue;
                    }

                    if (TryAttack(pirate))
                        continue;

                    if (_leaderDestination == null)
                    {
                        Sail(_leader.Location, pirate);
                    }
                    else
                    {
                        Sail(_leaderDestination, pirate);
                    }
                }
            }

            HandleDrones();
            HandleDecoy();
        }

        private int GetNumberOfFriendlyPiratesInLeaderSpot()
        {
            int numberOfFriendlyPiratesInLeaderSpot = 0;
            foreach (Pirate friendlyPirate in _game.GetMyLivingPirates())
            {
                if (friendlyPirate.Distance(_leader) == 0)
                {
                    numberOfFriendlyPiratesInLeaderSpot++;
                }
            }

            return numberOfFriendlyPiratesInLeaderSpot;
        }

        private void BeatMitsad()
        {
            Location edgeOfCenterIsland = new Location(22, 39);
            int leftiestColumn = 999;
            foreach (Pirate myPirate in _game.GetMyLivingPirates())
            {
                if (myPirate.Distance(edgeOfCenterIsland) < leftiestColumn)
                {
                    _leader = myPirate;
                    leftiestColumn = myPirate.Distance(edgeOfCenterIsland);
                }
            }
            Debug("leader: " + _leader.Id);


            bool done = false;

            if (IsCloseToEnemyPirate(_leader))
            {
                if (TryDecoy(_leader))
                    done = true;
            }

            if (!done)
            {
                if (TryAttack(_leader))
                    done = true;
            }

            if (!done)
            {
                Sail(edgeOfCenterIsland, _leader); //the closest spot in which you can conquer both islands
            }

            foreach (Pirate pirate in _game.GetMyLivingPirates())
            {
                if (pirate != _leader)//that pirate already did something
                {
                    if (IsCloseToEnemyPirate(pirate))
                    {
                        if (TryDecoy(pirate))
                            continue;
                    }

                    if (TryAttack(pirate))
                        continue;

                    Sail(edgeOfCenterIsland, pirate);
                }
            }

            HandleDrones();
            HandleDecoy();
        }

        public enum TypeOfAircraft
        {
            Pirates,
            Aircrafts
        }

        /// <summary>
        /// <para>Pair containing:</para>
        /// <para>A Mapobject </para>
        /// <para>An integer representing its distance from something </para>
        /// </summary>
        public class DistObjPair
        {
            public int distance { get; set; }
            public MapObject obj { get; set; }
            public DistObjPair(int dist, MapObject o)
            {
                distance = dist;
                obj = o;
            }

        }

        public class IslandPiratePair
        {
            public int pirateId;
            public Island island;
            public IslandPiratePair(int pirateId, Island island)
            {
                this.pirateId = pirateId;
                this.island = island;
            }
        }

        public class CityPiratePair
        {
            public int pirateId;
            public City city;
            public CityPiratePair(int pirateId, City city)
            {
                this.pirateId = pirateId;
                this.city = city;
            }
        }
        //
        public class DroneTank
        {
            public List<Drone> theDrones { get; set; }
            public bool quarter2ArrivedInTempDest { get; set; }
            public bool quarter3ArrivedInTempDest { get; set; }
            public DroneTank()
            {
                this.quarter2ArrivedInTempDest = false;
                this.quarter3ArrivedInTempDest = false;
            }
        }
    }
}