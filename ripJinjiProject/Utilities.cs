﻿using System.Collections.Generic;
using Pirates;

namespace MyBot
{
    /*
     * Pair Class - class used to pair between two object
     * 
     * ----- Generic -----
     * 
     * T, U - Types of the objectts
     * 
     * ----- Functions -----
     * 
     * Compare - A function for comparing two object of this class. Created for sorting a list in increasing order of this object
     * override Equals & Equals - Functions used to compare between two pair objects
     * GetHashCode - Useless function, overriden just so there wouldn't be warnings
    */
    public class Pair<T, U> : IComparer<Pair<T, U>>
    {
        public Pair() { }

        public Pair(T first, U second)
        {
            this.First = first;
            this.Second = second;
        }

        public int Compare(Pair<T, U> x, Pair<T, U> y)
        {
            return Comparer<T>.Default.Compare(x.First, y.First);
        }

        public override int GetHashCode()
        {
            if (this.Second == null)
                return -1;

            GameObject uObj = this.Second as GameObject;

            if (uObj == null)
                return -1;

            return uObj.GetHashCode();
        }

        public T First { get; set; }
        public U Second { get; set; }
    };

    /*
     * This class is used when using SortedList to get the list sorted by descending order
     * 
     * ----- Example -----
     * 
     * SortedList< Key, Value > newList = new SortedList< Key, Value > ( new DescComparer<#Key>() );
     * 
    */
    public class DescComparer<T> : IComparer<T>
    {
        public int Compare(T x, T y)
        {
            return Comparer<T>.Default.Compare(y, x);
        }
    }

    /*
     * This class is used when using SortedList to get the list sorted by increasing order
     * 
     * ----- Example -----
     * 
     * SortedList< Key, Value > newList = new SortedList< Key, Value > ( new IncrComparer<#Key>() );
     * 
    */
    public class IncrComparer<T> : IComparer<T>
    {
        public int Compare(T x, T y)
        {
            return Comparer<T>.Default.Compare(x, y);
        }
    }

    /*
     * This class is used for distance calculation.
     * 
     * ----- Generic -----
     * 
     * Object: 
     * where : MapObject - means it must be or derive from MapObject.
     * 
     * Target:
     * where : MapObject - means it must be or derive from MapObject.
     * 
     * ----- Functions -----
     * 
     * ObjDistFromTargets - Gets a list of targets, and an Object. It returns a list sorted by distance from the targets
     * 
     * FindClosestTarget - Gets a list of targets, and an Object. Returns the closest target
     * 
    */
    public class Distance<Object, Target> where Object : MapObject where Target : MapObject
    {
        public static List<Pair<int, Target>> ObjDistFromTargets(Object obj, List<Target> targets)
        {
            // Container to hold the results of the func
            List<Pair<int, Target>> res = new List<Pair<int, Target>>();

            // Find dist from every Target to object, and add it to the list
            foreach (Target target in targets)
            {
                res.Add(new Pair<int, Target>(obj.Distance(target), target));
            }

            // Sort the list
            res.Sort(new Pair<int, Target>());

            // Return the list
            return res;
        }

        public static Target FindClosestTarget(Object obj, List<Target> targets)
        {
            // Checks if the list is empty || obj is  null
            if (obj == null || targets.Count == 0)
                return null;

            // The closest target and the distance
            int dist = 10000;
            Target closeTarget = null;

            // Find closest target
            foreach (Target target in targets)
            {
                // Check if the current target exists
                if (target == null)
                    continue;

                // Check if the current target is closer
                if (obj.Distance(target) < dist)
                {
                    dist = obj.Distance(target);
                    closeTarget = target;
                }
            }

            // Return closest target
            return closeTarget;
        }
    }
}
