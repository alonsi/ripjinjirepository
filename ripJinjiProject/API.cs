﻿using Pirates;
using System.Collections.Generic;

namespace MyBot
{
    /**
     *  Game Object
    **/

    class Game
    {
        // The main game object
        public PirateGame _nativeGame;

        // Enemy ships
        public List<EnemyShip> _enemyShips;

        // Ally drones
        public List<AllyDrone> _allyDrones;

        // Ally cities
        public List<AllyCity> _allyCities;

        // Every island which is NOT OURS
        public List<NeutralIsland> _neutralIslands;
        // Ally ships
        public List<AllyShip> _allyShips;

        public Game(PirateGame newGame)
        {
            // Initialize every List
            this._enemyShips = new List<EnemyShip>();
            this._allyCities = new List<AllyCity>();
            this._allyDrones = new List<AllyDrone>();
            this._allyShips = new List<AllyShip>();
            this._neutralIslands = new List<NeutralIsland>();

            // Set _game object with the new game
            this._nativeGame = newGame;

            // Set the list of enemyShips with the new enemy Pirate object
            foreach (Pirate pirate in this._nativeGame.GetEnemyLivingPirates())
            {
                this._enemyShips.Add(new EnemyShip(pirate));
            }

            // Set the list of allyDrones with the new AllyDrone object
            foreach (Drone drone in this._nativeGame.GetMyLivingDrones())
            {
                this._allyDrones.Add(new AllyDrone(drone));
            }

            // Set the list of allyCities with the new AllyCity object
            foreach (City city in this._nativeGame.GetMyCities())
            {
                this._allyCities.Add(new AllyCity(city));
            }

            // Set the list of every island which is NOT OURS
            foreach (Island island in this._nativeGame.GetNotMyIslands())
            { _neutralIslands.Add(new NeutralIsland(island)); }

            // Set the list of Ally ships
            foreach (Pirate pirate in this._nativeGame.GetMyLivingPirates())
            {
                this._allyShips.Add(new AllyShip(pirate));
            }

        }
    }

    /**
     *  Enemy Classes
    **/

    // Enemy Ship
    class EnemyShip : MapObject
    {
        // Current Ship
        public Pirate _nativeShip;

        // Is the ship attacked
        public bool _isAttacked;

        // Returns the current location of the object
        public override Location GetLocation()
        {
            return this._nativeShip.GetLocation();
        }

        // Basic constructor
        public EnemyShip(Pirate newShip) : base()
        {
            this._nativeShip = newShip;
            this._isAttacked = false;
        }
    }

    // Enemy Drone
    class EnemyDrone : MapObject
    {
        // Current Ship
        public Drone _nativeDrone;
        // Returns the location of the current object
        public override Location GetLocation()
        {
            return this._nativeDrone.GetLocation();
        }

        // Basic constructor
        public EnemyDrone(Drone newDrone) : base()
        {
            this._nativeDrone = newDrone;
        }
    }

    // Enemy City
    class EnemyCity : MapObject
    {
        // Current Ship
        public City _nativeCity;

        // Is the city going to be conquered by an Ally ship
        public AllyShip _conquer;

        // Returns the current location of the object
        public override Location GetLocation()
        {
            return this._nativeCity.GetLocation();
        }

        // Basic constructor
        public EnemyCity(City newCity) : base()
        {
            this._nativeCity = newCity;
        }
    }

    /**
     *  Ally Classes
    **/

    // Ally Ship
    class AllyShip : MapObject
    {
        // Current Ship
        public Pirate _nativeShip;

        // Is the ship doing something
        public bool _acting;

        // Returns the current location of the object
        public override Location GetLocation()
        {
            return this._nativeShip.GetLocation();
        }

        // Basic constructor
        public AllyShip(Pirate newShip) : base()
        {
            this._nativeShip = newShip;
            this._acting = false;
        }
    }

    // Ally Drone
    class AllyDrone : MapObject
    {
        // Current Drone
        public Drone _nativeDrone;

        // Is the drone going to a city
        public AllyCity _targetCity;

        // Returns the current location of the object
        public override Location GetLocation()
        {
            return this._nativeDrone.GetLocation();
        }

        // Basic constructor
        public AllyDrone(Drone newDrone) : base()
        {
            this._nativeDrone = newDrone;
        }
    }

    // Ally Cities
    class AllyCity : MapObject
    {
        // Ally drones who are going to this city
        public List<AllyDrone> _drones;

        // Add a drone to the city
        void addDrone(AllyDrone drone)
        {
            if (drone._targetCity == this)
                this._drones.Add(drone);
        }

        // Current Drone
        public City _nativeCity;

        // Returns the current location of the object
        public override Location GetLocation()
        {
            return this._nativeCity.GetLocation();
        }

        // Basic constructor
        public AllyCity(City newCity) : base()
        {
            this._nativeCity = newCity;
            this._drones = new List<AllyDrone>();
        }
    }

    /**
     *  Neutral Classes
    **/

    // Neutral City
    class NeutralCity : MapObject
    {
        // Current Ship
        public City _nativeCity;
        // Is the city going to be conquered by an Ally ship
        public AllyShip _conquer;

        // Returns the current location of the object
        public override Location GetLocation()
        {
            return this._nativeCity.GetLocation();
        }

        // Basic constructor
        public NeutralCity(City newCity)
        {
            this._nativeCity = newCity;
        }
    }

    // Neutral Island
    class NeutralIsland : MapObject
    {
        // Current Island
        public Island _nativeIsland;

        // Is the island going to be conquered by an Ally ship
        public AllyShip _conquer;

        // Returns the current location of the object
        public override Location GetLocation()
        {
            return this._nativeIsland.GetLocation();
        }

        // Basic constructor
        public NeutralIsland(Island newIsland)
        {
            this._nativeIsland = newIsland;
            this._conquer = null;
        }
    }
}
