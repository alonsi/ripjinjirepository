﻿using System.Collections.Generic;
using Pirates;

namespace MyBot
{
    class ActionManager
    {
        // The game object
        Game _game;

        // Basic Constructor
        public ActionManager(Game game)
        {
            _game = game;
        }

        // Action main function, this function will cause every event to be treated
        public void TreatEvents(EventManager manager)
        {
            foreach (Event e in manager._events)
            {
                e.ChooseAction(this, _game);
            }
        }
    }

    /**
     * The basic class of the actions, supposed to contain functions and variables which are needed for every action
    **/
    abstract class Action
    {
        // Virtual Act function, will be overriden to be every action for every event
        public virtual void Act(Event temp, Game game) { }
    }

    /**
     * Action - OUR ships need to conquer islands
    **/
    class ConquerIslandAction : Action
    {
        // Basic Constructor
        public ConquerIslandAction() { }

        // The Action for ConquerIslandEvent
        public override void Act(Event temp, Game game)
        {
            // The given Event is of ConquerIslandEvent type, so we convert it to that type
            ConquerIslandEvent e = temp as ConquerIslandEvent;

            if (e == null)
                return;

            /*
             * This function goes through every ship and finds an island to sail to.
             * It checks if there's a ship closer to an island, and if there's then the current ship will sail to that island.
             * If the ship is the closest ship WHICH DOES NOTHING (it was not given an action) to an island, then it will sail to it.
            */
            for (int round = 0; round < game._allyShips.Count; round++)
            {
                for (int allyIndex = 0; allyIndex < game._allyShips.Count; allyIndex++)
                {
                    AllyShip ally = game._allyShips[allyIndex];

                    if (ally == null)
                        continue;

                    // Check if ally is already doing an action
                    if (ally._acting)
                        continue;

                    // Find the closest island to conquer
                    List<Pair<int, NeutralIsland>> distFromNeutral = Distance<AllyShip, NeutralIsland>.ObjDistFromTargets(ally, e._conquerNeutral);

                    // Check which island the ally is the closest to
                    for (int islandIndex = 0; islandIndex < distFromNeutral.Count; islandIndex++)
                    {
                        Pair<int, NeutralIsland> island = distFromNeutral[islandIndex];

                        // Check if some ship is already conquering the island
                        if (island.Second._conquer != null)
                        {
                            // Then remove it
                            distFromNeutral.Remove(island);
                            continue;
                        }

                        // Check if there's a closer ally ship to the island
                        foreach (AllyShip checkAlly in game._allyShips)
                        {
                            // Check if this is the same ally
                            if (checkAlly == ally || checkAlly._acting || checkAlly == null)
                                continue;

                            // Remove an island from the ship's list if the current checkAlly is closer to the island
                            if (island.Second._nativeIsland.Distance(checkAlly._nativeShip) < island.First)
                            {
                                distFromNeutral.Remove(island);
                                break;
                            }
                        }
                    }

                    // If there's an island, sail to it
                    for (int islandIndex = 0; islandIndex < distFromNeutral.Count; islandIndex++)
                    {
                        Pair<int, NeutralIsland> island = distFromNeutral[islandIndex];

                        // Get sail options to island
                        List<Location> sail_spots = game._nativeGame.GetSailOptions(ally._nativeShip, island.Second);

                        // Check if there's a spot to sail to
                        if (sail_spots.Count == 0)
                            continue;

                        // Sail to the first option
                        game._nativeGame.SetSail(ally._nativeShip, sail_spots[0]);

                        ally._acting = true;

                        island.Second._conquer = ally;

                        // Stop attempting to sail
                        distFromNeutral.Clear();
                    }
                }
            }
        }
    }
}
