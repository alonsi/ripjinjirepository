﻿using System.Collections.Generic;
using Pirates;

namespace MyBot
{
    class EventManager
    {
        // List of events which will be treated 

        public List<Event> _events { private set; get; }

        // Basic constructor
        public EventManager()
        {
            this._events = new List<Event>();
        }

        // Checks events
        public void CheckEvents(Game game)
        {
            // Check event - Conquer islands
            ConquerIslandEvent conquerIsland = new ConquerIslandEvent();
            conquerIsland.CheckEvent(game);

            if (conquerIsland._value)
                this._events.Add(conquerIsland);
        }
    }

    /**
     * The basic class of the events, supposed to contain functions and variables which are needed for every Event
    **/
    class Event
    {
        // The name of the event
        private const string _name = "BasicEvent";

        // True - the event is relevant
        public bool _value;

        // Return the name of the object
        public virtual string GetName() { return Event._name; }

        // Constructor of the class Event
        public Event()
        {
            this._value = false;
        }

        // This function checks whether the event is relevant or not
        public virtual void CheckEvent(Game game) { }

        // This function calls the fitting action for the event
        public virtual void ChooseAction(ActionManager actionManager, Game game) { }
    }

    // *******************************************************************************************************************************************

    /**
     * Event - one of the enemy ships is sieging one of OUR cities
    **/
    class EnemySiegeEvent : Event
    {
        // The islands which are sieged by enemies
        public List<AllyCity> _siegedCities;

        // Constructor of the class Event
        public EnemySiegeEvent() { }

        // This function checks whether the event is relevant or not
        public override void CheckEvent(Game game)
        {
            List<EnemyShip> enemies = game._enemyShips;
            this._siegedCities = new List<AllyCity>();

            // Checks every ENEMY ship on every ALLY city
            for (int i = 0; i < enemies.Count; i++)
            {
                foreach (AllyCity city in game._allyCities)
                {
                    // A destroyer is needed only if there are friendly drones in their way to our city
                    if (city._drones.Count == 0)
                        continue;

                    // Checks if the ENEMY ship is in range of the city
                    if (enemies[i]._nativeShip.GetLocation().InRange(city._nativeCity, enemies[i]._nativeShip.AttackRange + 3))
                    {
                        if (!this._siegedCities.Contains(city))
                        {
                            this._siegedCities.Add(city);
                        }
                        this._value = true;
                    }
                }
            }
        }

        // This function calls the fitting action for the event
        public override void ChooseAction(ActionManager actionManager, Game game)
        {

        }
    }

    /**
     * Event - one of OUR ships needs to siege one of the enemy island
    **/
    class AllySiegeEvent : Event
    {
        // Constructor of the class Event
        public AllySiegeEvent() { }

        // This function checks whether the event is relevant or not
        public override void CheckEvent(Game game)
        {

        }

        // This function calls the fitting action for the event
        public override void ChooseAction(ActionManager actionManager, Game game)
        {

        }
    }

    /**
     * Event - one of OUR islands will be attacked by an ENEMY ship
    **/
    class DefendIslandEvent : Event
    {
        // Constructor of the class Event
        public DefendIslandEvent() { }

        // This function checks whether the event is relevant or not
        public override void CheckEvent(Game game)
        {

        }

        // This function calls the fitting action for the event
        public override void ChooseAction(ActionManager actionManager, Game game)
        {

        }
    }

    /**
     * Event - one of OUR ships needs to conquer a NEUTRAL/ENEMY island
    **/
    class ConquerIslandEvent : Event
    {
        // The name of the Event
        private const string _name = "ConquerIslandEvent";

        // Return the name of the object
        public override string GetName() { return ConquerIslandEvent._name; }

        // Lists of cities which need to be conquered
        public List<NeutralIsland> _conquerNeutral;

        // Constructor of the class Event
        public ConquerIslandEvent()
        {
            this._conquerNeutral = new List<NeutralIsland>();
        }

        // This function checks whether the event is relevant or not
        public override void CheckEvent(Game game)
        {
            // #TODO - When int/float vaules are added, add parameters to decide which islands are best to conquer
            foreach (NeutralIsland island in game._neutralIslands)
            {
                _conquerNeutral.Add(island);
            }

            if (this._conquerNeutral.Count > 0)
                this._value = true;
        }

        // This function calls the fitting action for the event
        public override void ChooseAction(ActionManager actionManager, Game game)
        {
            ConquerIslandAction action = new ConquerIslandAction();

            action.Act(this, game);
        }
    }

    /**
     * Event - OUR drones need
    **/
    class DefendDroneEvent : Event
    {

    }
}
